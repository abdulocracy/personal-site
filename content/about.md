---
title: "about"
date: 2019-11-04
---

<image src="face.jpg" width="192" height="192" />

- name: Abdulkadir Furkan Şanlı
- handle: abdulocracy
- contact:
  - email: me@abdulocra.cy
    - gpg: [0xEE6ED1FE](key.asc)
  - matrix: @me:abdulocra.cy
  - irc (libera.chat): abdulocracy
  - mastodon: <a rel="me" href="https://toot.abdulocra.cy/@abdulocracy">@abdulocracy@abdulocra.cy</a>
